[git]:/repo_assets/git_commands.png "Cheat Sheet"
[step_1]:repo_assets/step_1.PNG "Step 1"
[step_2]:repo_assets/step_2.PNG "Step 2"
[step_3]:repo_assets/step_3.PNG "Step 3"
[step_4]:repo_assets/step_4.PNG "Step 4"
[step_5]:repo_assets/step_5.PNG "Step 5"
[step_6]:repo_assets/step_6.PNG "Step 6"
# Setting up source tree
1. [Just Click me to Download source tree](https://product-downloads.atlassian.com/software/sourcetree/windows/ga/SourceTreeSetup-3.0.17.exe)
2. install and click bitbucket cloud setup
3. click clone then click open in source tree
3.5 its click browse from your account
4. below is a cheat sheet on how git works

![sorry][git]
# Saving to the repo/cloud
#### 0. save any files in nlogo or whatever tool you are using
#### 1. Pull
	
![][step_1]
>this will update your files you may need to fix any merge conflicts

#### 2. tell source tree you updated your files including new files
	
![][step_2]
>adding new files will be unlikely due to netlogo doing eveything in one files

#### 3. stage files and look at your changes
	
![][step_3]
> you can see the indidual update

#### 4. add a usefull message to your changes

![][step_4]
>when to commit is highly debated in the programing community some choose 
to commit at the end of the day
while others demand you commit when you implement any logical feature of the software
when can decide if we need a rigurious guide on how to do things if neccesary


#### 5. finilize the commit
	
![][step_5]
> we are not in the cloud yet unless you hit that checkbox

#### 6. push into the repo

![][step_6]
>pat yourself on the back!
you did not have to email this file and now you are a git master.
