import noise
import random
import os
from pathlib import Path
import numpy as np
from PIL import Image
#import matplotlib
#from threading import Thread

def make(side = 128, octaves = 6, persistence = 0.5, lacunarity = 2.0, fString="map", seed=5):
	shape = (side, side)
	world = np.zeros(shape)
	random.seed(5)
	foo = open(fString + ".csv", "w+")
	noise.randint_function = random.randint
	for i in range(shape[0]):
		for j in range(shape[1]):
			baz = noise.pnoise2(
				i/side,
				j/side,
				octaves=octaves,
				persistence=persistence,
				lacunarity=lacunarity,
				repeatx=side,
				repeaty=side,
				base=0
			)
			world[i][j] = baz
		bar = list(map(lambda x: str(x) + ", ", world[i]))
		bar = ''.join(bar)
		bar = bar[0: -2]
		foo.write(bar + "\n")
	foo.close()
	fString += ".tiff"
	
	if Path(fString).is_file():
		os.remove(fString)

	im = Image.fromarray(world)
	im.save(fString)

#i = 0, scale = 100.0, octaves = 6, persistence = 0.5, lacunarity = 2.0, seed=5000
if False:
	make(0, 1024, 1)
	make(1, 1024, 2)
	make(2, 1024, 4)
	make(3, 1024, 6)

make(128, 4, .5, 2.3, 'perlin', 100)
